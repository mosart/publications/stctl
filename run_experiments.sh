#!/bin/bash

################################################
# Script for generating experiments table      #
# Author: Laure Petrucci                       #
# Version: 1.0                                 #
# Date: 2022-10-22                             #
################################################

function usage {
  echo -e "\033[1;31musage\033[0m: $0 [-h | [-v maxvoters] [-a maxagents] [-c maxcandidates] [-s one|all] [-t timeout] [-x] -o table_filename]"
}

function help {
  echo -e ""
  usage
  echo -e "\nExecutes the experiments.\nThe result is written in the file specified with the \033[1m-o\033[0m option"
  echo -e "\n\033[1m-h\033[0m\t\tThis help"
  echo -e "\n\033[1m-v maxvoters\033[0m\tUses a specified value for the maximal number of voters \033[4m[default: $maxvoters]\033[0m"
  echo -e "\n\033[1m-a maxagents\033[0m\tUses a specified value for the maximal number of agents \033[4m[default: $maxagents]\033[0m"
  echo -e "\n\033[1m-c maxcandidates\033[0m Uses a specified value for the maximal number of candidates \033[4m[default: $maxcandidates]\033[0m"
  echo -e "\n\033[1m-s strategy\033[0m\tFind one strategy or all of them \033[4m[default: one]\033[0m"
  echo -e "\n\033[1m-t timeout\033[0m\tUses a specified value for the timeout (in seconds) \033[4m[default: $timeout]\033[0m"
  echo -e "\n\033[1m-x\033[0m\t\tUses an optimisation \033[4m[default: $optim_gen]\033[0m"
  exit
}

function process_results {
  exceeds_time=$(grep "time limit" $one_result)
  if [ -z "$exceeds_time" ]; then
    echo -n $(grep "completed after" $one_result |
      sed -e 's/\[EF\] Algorithm completed after //' |
      sed -e 's/ seconds.//' | sed -e 's/ second.//' |
      sed -e 's/\[0m//') >>$output_file
  else
    echo -n "TO" >>$output_file
  fi
}

# main part of the script
imitator="./tools/imitator/imitator-$(uname)-$(uname -m)"

if ! command -v $imitator &>/dev/null; then
  echo "Imitator cannot be found. Please set its PATH in the imitator variable"
  exit
fi

# get the options
timeout=120 # 2 minutes by default
maxvoters=3 # default max values for generation
maxcandidates=4
maxagents=2
output_file=
strategy=one
optim_gen='false'

while getopts "ht:o:v:a:c:s:x" opt; do
  case $opt in
  h) help ;;
  t) timeout=$OPTARG ;;
  o) output_file=$OPTARG ;;
  v) maxvoters=$OPTARG ;;
  a) maxagents=$OPTARG ;;
  c) maxcandidates=$OPTARG ;;
  s) strategy=$OPTARG ;;
  x) optim_gen='true' ;;
  esac
done

if [ -z "$output_file" ]; then
  usage
  exit
fi

# select generator
if ${optim_gen}; then
  generator="./tools/voters_generator/optim_generator"
  name_base="candOptStrat"
else
  generator="./tools/voters_generator/generator"
  name_base="candStrat"
fi

if ! command -v $generator &>/dev/null; then
  echo "compiling the generator tool ..."
  (cd tools/voters_generator && make)
fi

imitator_options="-no-output-result -merge=none -time-limit=$timeout"
extension=".${output_file##*.}"
one_result="$(basename $output_file $extension).tmp"
rm -f $one_result
rm -f $output_file
# print the command line that was used
echo $0 $* >$output_file
# print csv header
echo '' >>$output_file
echo -n "agents;strategy;timeout;voters" >>$output_file
for ((candidates = 1; candidates <= maxcandidates; candidates++)); do
  echo -n ";candidate_$candidates" >>$output_file
done
# table
for ((agents = 1; agents <= maxagents; agents++)); do
  echo -e "\033[1;31m$agents agents, $strategy strategies, timeout ${timeout}s\033[0m"

  # print csv values
  for ((voters = agents; voters <= maxvoters; voters++)); do
    echo -e "\033[1;31mRunning experiments for model $voters voters\033[0m"
    echo '' >>$output_file
    echo -n "$agents;$strategy;$timeout;$voters" >>$output_file
    for ((candidates = 1; candidates <= maxcandidates; candidates++)); do
      echo -n ';' >>$output_file
      $generator -v $voters -c $candidates -a $agents -s $strategy
      $imitator $imitator_options \
        ${voters}voter${candidates}${name_base}${agents}agents.imi ${agents}votersT.imiprop \
        >$one_result 2>$one_result
      process_results
    done
  done
done

rm -f $one_result

# move all the results to their folders
mv *imi *.imiprop case_studies/
mv $output_file results/
