/* Program to generate scaling voter example */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/* functions defined in this program */
void writeAuthority(FILE *modelfile);
void writeVoter(FILE *modelfile, int numvoter);
void writeImitatorModel();
void writeImitatorProperty();

/* Global variables used for the characteristics of the generation */
int nbvoters = 0, nbcandidates = 0,
    nbagents = 0;                 /* parameters for the generation */
char synthesis[10] = {'\0'};      /* synthesize all or one strategy */
char modelfilename[100] = {'\0'}; /* the model file to write */
char propfilename[100] = {'\0'};  /* the property file to write */
char cmdline[100];

/* main program */
int main(int argc, char *argv[]) {
  strcpy(synthesis, "all");
  /* check the options passed */
  int opt, count;

  for (count = 0; count < argc; count++) {
    strcat(cmdline, argv[count]);
    strcat(cmdline, " ");
  }
  while ((opt = getopt(argc, argv, "v:c:a:s:h")) != -1) {
    switch (opt) {
      case 'v':
        nbvoters = atoi(optarg);
        break;
      case 'c':
        nbcandidates = atoi(optarg);
        break;
      case 'a':
        nbagents = atoi(optarg);
        break;
      case 'h':
        printf(
            "Usage: %s [-v nbvoters] [-a nbagents] [-c nbcandidates] [-s "
            "all|one] [-h]\n",
            argv[0]);
        printf("where:\nnbvoters is the number of voters\n");
        printf(
            "nbagents is the number of voters for which a strategy must be "
            "found\n");
        printf(
            "nbcandidates is the number of candidates running in the "
            "election\n");
        printf("all strategies or only one are synthesized\n");
        printf("-h prints this help\n");
        return 0;
      case 's':
        strcpy(synthesis, optarg);
        if (strcmp(synthesis, "one") && strcmp(synthesis, "all")) {
          printf("Error: synthesis must be all or one\n");
          return 1;
        }
        break;
      case ':':
        printf("option needs a value\n");
        return 1;
      case '?':
        printf("unknown option: %c\n", optopt);
        return 1;
    }
  }
  if (nbagents > nbvoters) {
    printf("Error: More agents than voters\n");
    return 1;
  }
  /* optind is for the extra arguments which are not parsed */
  for (; optind < argc; optind++) {
    printf("extra arguments: %s\n", argv[optind]);
  }
  if (nbvoters == 0) {
    printf("using default of 2 voters\n");
    nbvoters = 2;
  }
  if (nbcandidates == 0) {
    printf("using default of 2 candidates\n");
    nbcandidates = 2;
  }
  if (nbagents == 0) {
    printf("using default of 1 agent\n");
    nbagents = 1;
  }
  /* define the files names */
  sprintf(modelfilename, "%dvoter%dcandStrat%dagents.imi", nbvoters,
          nbcandidates, nbagents);
  sprintf(propfilename, "%dvotersT.imiprop", nbagents);
  printf("Generating model with %d voters, %d candidates ", nbvoters,
         nbcandidates);
  printf("and property for %d agents ", nbagents);
  printf("in files %s and %s\n", modelfilename, propfilename);
  printf("synthesis of strategies: %s\n", synthesis);
  writeImitatorModel();
  writeImitatorProperty();
  return 0;
} /* end main */

/* Write the Imitator model */
void writeImitatorModel() {
  FILE *thefile;
  time_t current_time;
  char *c_time_string;
  int countAgents, countCandidates, countVoters;

  thefile = fopen(modelfilename, "w");
  /* write header */
  fprintf(thefile, "(* IMITATOR file automatically generated with *)\n");
  fprintf(thefile, "(* %s *)\n", cmdline);
  current_time = time(NULL);
  c_time_string = strtok(ctime(&current_time), "\n");
  fprintf(thefile, "(* Created: %s *)\n\n", c_time_string);
  fprintf(thefile, "var t, x: clock; (* global time and delay *)\n");
  /* variables for voters' strategies */
  countAgents = 1;
  fprintf(thefile, "\t(* strategies for voter %d *)\n", countAgents);
  fprintf(thefile, "\tregm%d, regi%d, regp%d,\n", countAgents, countAgents,
          countAgents);
  for (countCandidates = 1; countCandidates < nbcandidates; countCandidates++)
    fprintf(thefile, "\tvm%d%d, vi%d%d, vp%d%d,\n", countAgents,
            countCandidates, countAgents, countCandidates, countAgents,
            countCandidates);
  fprintf(thefile, "\tvm%d%d, vi%d%d, vp%d%d", countAgents, countCandidates,
          countAgents, countCandidates, countAgents, countCandidates);
  for (countAgents = 2; countAgents <= nbagents; countAgents++) {
    fprintf(thefile, ",\n\t(* strategies for voter %d *)\n", countAgents);
    fprintf(thefile, "\tregm%d, regi%d, regp%d,\n", countAgents, countAgents,
            countAgents);
    for (countCandidates = 1; countCandidates < nbcandidates; countCandidates++)
      fprintf(thefile, "\tvm%d%d, vi%d%d, vp%d%d,\n", countAgents,
              countCandidates, countAgents, countCandidates, countAgents,
              countCandidates);
    fprintf(thefile, "\tvm%d%d, vi%d%d, vp%d%d", countAgents, countCandidates,
            countAgents, countCandidates, countAgents, countCandidates);
  }
  fprintf(thefile, "\n\t: parameter;\n\n");

  /* Write the automata */
  writeAuthority(thefile);
  for (countVoters = 1; countVoters <= nbvoters; countVoters++)
    writeVoter(thefile, countVoters);

  /* write the initial state */
  fprintf(thefile, "\n(*****************)\n(* initial state *)\n");
  fprintf(thefile, "(*****************)\ninit := {\n");
  /* automata initial locations */
  fprintf(thefile, "\tdiscrete =\n\t\tloc[authority] := a0");
  for (countVoters = 1; countVoters <= nbvoters; countVoters++)
    fprintf(thefile, ",\n\t\tloc[voter%d] := v%d0", countVoters, countVoters);
  /* timing */
  fprintf(thefile, ";\n\tcontinuous =\n");
  fprintf(thefile, "\t\t(* clocks *)\n\t\tx >= 0 & t = 0 &\n");
  /* strategies */
  fprintf(thefile, "\t\t(* strategies *)\n");
  for (countAgents = 1; countAgents < nbagents; countAgents++) {
    fprintf(thefile, "\t\t(* voter %d *)\n", countAgents);
    fprintf(thefile, "\t\tregm%d >= 0 & regi%d >= 0 & regp%d >= 0 &\n",
            countAgents, countAgents, countAgents);
    for (countCandidates = 1; countCandidates <= nbcandidates;
         countCandidates++)
      fprintf(thefile, "\t\tvm%d%d >= 0 & vi%d%d >= 0 & vp%d%d >= 0 &\n",
              countAgents, countCandidates, countAgents, countCandidates,
              countAgents, countCandidates);
  }
  /* strategy of last agent */
  fprintf(thefile, "\t\t(* voter %d *)\n", countAgents);
  fprintf(thefile, "\t\tregm%d >= 0 & regi%d >= 0 & regp%d >= 0 &\n",
          countAgents, countAgents, countAgents);
  for (countCandidates = 1; countCandidates < nbcandidates; countCandidates++)
    fprintf(thefile, "\t\tvm%d%d >= 0 & vi%d%d >= 0 & vp%d%d >= 0 &\n",
            countAgents, countCandidates, countAgents, countCandidates,
            countAgents, countCandidates);
  fprintf(thefile, "\t\tvm%d%d >= 0 & vi%d%d >= 0 & vp%d%d >= 0", countAgents,
          countCandidates, countAgents, countCandidates, countAgents,
          countCandidates);
  fprintf(thefile, "\n\t;\n}\nend\n");
  /* finished writing */
  fclose(thefile);
} /* end writeImitatorModel */

/* Write the Authority automaton */
void writeAuthority(FILE *modelfile) {
  int countVoters, countCandidates;

  fprintf(modelfile, "(* The authority *)\nautomaton authority\nsynclabs:");
  /* synchronised transitions with the voters */
  for (countVoters = 1; countVoters <= nbvoters; countVoters++) {
    fprintf(modelfile, "\tregm%d, regi%d, regp%d,\n", countVoters, countVoters,
            countVoters);
    fprintf(modelfile, "\tpackm%d, packi%d, packp%d,\n", countVoters,
            countVoters, countVoters);
    for (countCandidates = 1; countCandidates <= nbcandidates;
         countCandidates++)
      fprintf(modelfile, "\tvm%d%d, vi%d%d, vp%d%d,\n", countVoters,
              countCandidates, countVoters, countCandidates, countVoters,
              countCandidates);
  }
  /* end of the voting process */
  fprintf(modelfile, "\tclose;\n\n");
  /* Initial location */
  fprintf(modelfile, "loc a0: invariant t <= 11\n");
  fprintf(modelfile, "(* start by registering a modality for a voter *)\n");
  /* registration loops for each voter */
  for (countVoters = 1; countVoters <= nbvoters; countVoters++) {
    fprintf(modelfile, "\t(* voter %d *)\n", countVoters);
    fprintf(modelfile, "\twhen t < 1\n\t\tsync regm%d do {x:=0} goto m%d;\n",
            countVoters, countVoters);
    fprintf(modelfile, "\twhen t < 6\n\t\tsync regi%d do {x:=0} goto i%d;\n",
            countVoters, countVoters);
    fprintf(modelfile, "\twhen t < 10\n\t\tsync regp%d do {x:=0} goto p%d;\n",
            countVoters, countVoters);
  }
  /* voting process */
  fprintf(modelfile, "(* receive votes *)\n");
  for (countVoters = 1; countVoters <= nbvoters; countVoters++) {
    fprintf(modelfile, "\t(* voter %d *)\n", countVoters);
    for (countCandidates = 1; countCandidates <= nbcandidates;
         countCandidates++) {
      fprintf(modelfile, "\twhen 1 <= t & t < 7 sync vm%d%d goto a0;\n",
              countVoters, countCandidates);
      fprintf(modelfile, "\twhen 6 <= t & t < 9 sync vi%d%d goto a0;\n",
              countVoters, countCandidates);
      fprintf(modelfile, "\twhen 10 <= t & t < 11 sync vp%d%d goto a0;\n",
              countVoters, countCandidates);
    }
  }
  /* ending the vote */
  fprintf(modelfile, "(* close the vote *)\n");
  fprintf(modelfile, "\twhen t = 11 sync close goto closed;\n");

  /* intermediate locations for sending the appropriate voting package */
  fprintf(modelfile, "(* send the voting package *)\n");
  for (countVoters = 1; countVoters <= nbvoters; countVoters++) {
    fprintf(modelfile, "(* voter %d *)\n", countVoters);
    fprintf(modelfile, "loc m%d: invariant x<=0\n", countVoters);
    fprintf(modelfile, "\twhen x=0 sync packm%d goto a0;\n", countVoters);
    fprintf(modelfile, "loc i%d: invariant x<=0\n", countVoters);
    fprintf(modelfile, "\twhen x=0 sync packi%d goto a0;\n", countVoters);
    fprintf(modelfile, "loc p%d: invariant x<=0\n", countVoters);
    fprintf(modelfile, "\twhen x=0 sync packp%d goto a0;\n", countVoters);
  }
  /* the vote is closed */
  fprintf(modelfile, "(* the vote is closed *)\n");
  fprintf(modelfile, "loc closed: invariant True\nend\n\n");
} /* end writeAuthority */

/* Write a voter automaton */
void writeVoter(FILE *modelfile, int numvoter) {
  int countCandidates, myCandidate;

  fprintf(modelfile, "(* Voter %d *)\nautomaton voter%d\n", numvoter, numvoter);
  /* synchronised transitions with the authority */
  fprintf(modelfile, "synclabs: regm%d, regi%d, regp%d,\n", numvoter, numvoter,
          numvoter);
  fprintf(modelfile, "\tpackm%d, packi%d, packp%d,\n", numvoter, numvoter,
          numvoter);
  for (countCandidates = 1; countCandidates < nbcandidates; countCandidates++)
    fprintf(modelfile, "\tvm%d%d, vi%d%d, vp%d%d,\n", numvoter, countCandidates,
            numvoter, countCandidates, numvoter, countCandidates);
  fprintf(modelfile, "\tvm%d%d, vi%d%d, vp%d%d;\n\n", numvoter, countCandidates,
          numvoter, countCandidates, numvoter, countCandidates);
  /* write the locations and their transitions */
  fprintf(modelfile, "(* start by registering a modality *)\n");
  fprintf(modelfile, "loc v%d0: invariant True\n", numvoter);
  /* if the voter is part of the agents in the formula,
  use the strategy parameters */
  if (numvoter <= nbagents) {
    fprintf(modelfile, "\twhen regm%d=1 & regi%d=0 & regp%d=0\n", numvoter,
            numvoter, numvoter);
    fprintf(modelfile, "\t\tsync regm%d goto m%d; (* vote by postal mail *)\n",
            numvoter, numvoter);
    fprintf(modelfile, "\twhen regm%d=0 & regi%d=1 & regp%d=0\n", numvoter,
            numvoter, numvoter);
    fprintf(modelfile, "\t\tsync regi%d goto i%d; (* vote by internet *)\n",
            numvoter, numvoter);
    fprintf(modelfile, "\twhen regm%d=0 & regi%d=0 & regp%d=1\n", numvoter,
            numvoter, numvoter);
    fprintf(modelfile,
            "\t\tsync regp%d goto p%d; (* vote at polling station *)\n",
            numvoter, numvoter);
  } else {
    fprintf(modelfile, "\twhen True\n");
    fprintf(modelfile, "\t\tsync regm%d goto m%d; (* vote by postal mail *)\n",
            numvoter, numvoter);
    fprintf(modelfile, "\twhen True\n");
    fprintf(modelfile, "\t\tsync regi%d goto i%d; (* vote by internet *)\n",
            numvoter, numvoter);
    fprintf(modelfile, "\twhen True\n");
    fprintf(modelfile,
            "\t\tsync regp%d goto p%d; (* vote at polling station *)\n",
            numvoter, numvoter);
  }

  fprintf(modelfile, "(* wait for voting package *)\n");
  fprintf(modelfile, "loc m%d: invariant True\n", numvoter);
  fprintf(modelfile, "\twhen True sync packm%d goto rm%d;\n", numvoter,
          numvoter);
  fprintf(modelfile, "loc i%d: invariant True\n", numvoter);
  fprintf(modelfile, "\twhen True sync packi%d goto ri%d;\n", numvoter,
          numvoter);
  fprintf(modelfile, "loc p%d: invariant True\n", numvoter);
  fprintf(modelfile, "\twhen True sync packp%d goto rp%d;\n", numvoter,
          numvoter);

  /* vote */
  fprintf(modelfile, "(* vote for a candidate *)\n");
  /* if the voter is part of the agents in the formula,
  use the strategy parameters */
  if (numvoter <= nbagents) {
    fprintf(modelfile, "loc rm%d: invariant True\n", numvoter);
    for (myCandidate = 1; myCandidate <= nbcandidates; myCandidate++) {
      fprintf(modelfile, "\twhen ");
      for (countCandidates = 1; countCandidates < nbcandidates;
           countCandidates++)
        if (myCandidate == countCandidates)
          fprintf(modelfile, "vm%d%d=1 & ", numvoter, countCandidates);
        else
          fprintf(modelfile, "vm%d%d=0 & ", numvoter, countCandidates);
      if (myCandidate == countCandidates)
        fprintf(modelfile, "vm%d%d=1 sync vm%d%d goto voted%d%d;\n", numvoter,
                countCandidates, numvoter, myCandidate, numvoter, myCandidate);
      else
        fprintf(modelfile, "vm%d%d=0 sync vm%d%d goto voted%d%d;\n", numvoter,
                countCandidates, numvoter, myCandidate, numvoter, myCandidate);
    }
    fprintf(modelfile, "loc ri%d: invariant True\n", numvoter);
    for (myCandidate = 1; myCandidate <= nbcandidates; myCandidate++) {
      fprintf(modelfile, "\twhen ");
      for (countCandidates = 1; countCandidates < nbcandidates;
           countCandidates++)
        if (myCandidate == countCandidates)
          fprintf(modelfile, "vi%d%d=1 & ", numvoter, countCandidates);
        else
          fprintf(modelfile, "vi%d%d=0 & ", numvoter, countCandidates);
      if (myCandidate == countCandidates)
        fprintf(modelfile, "vi%d%d=1 sync vi%d%d goto voted%d%d;\n", numvoter,
                countCandidates, numvoter, myCandidate, numvoter, myCandidate);
      else
        fprintf(modelfile, "vi%d%d=0 sync vi%d%d goto voted%d%d;\n", numvoter,
                countCandidates, numvoter, myCandidate, numvoter, myCandidate);
    }
    fprintf(modelfile, "loc rp%d: invariant True\n", numvoter);
    for (myCandidate = 1; myCandidate <= nbcandidates; myCandidate++) {
      fprintf(modelfile, "\twhen ");
      for (countCandidates = 1; countCandidates < nbcandidates;
           countCandidates++)
        if (myCandidate == countCandidates)
          fprintf(modelfile, "vp%d%d=1 & ", numvoter, countCandidates);
        else
          fprintf(modelfile, "vp%d%d=0 & ", numvoter, countCandidates);
      if (myCandidate == countCandidates)
        fprintf(modelfile, "vp%d%d=1 sync vp%d%d goto voted%d%d;\n", numvoter,
                countCandidates, numvoter, myCandidate, numvoter, myCandidate);
      else
        fprintf(modelfile, "vp%d%d=0 sync vp%d%d goto voted%d%d;\n", numvoter,
                countCandidates, numvoter, myCandidate, numvoter, myCandidate);
    }
  } else {
    fprintf(modelfile, "loc rm%d: invariant True\n", numvoter);
    for (countCandidates = 1; countCandidates <= nbcandidates;
         countCandidates++)
      fprintf(modelfile, "\twhen True sync vm%d%d goto voted%d%d;\n", numvoter,
              countCandidates, numvoter, countCandidates);
    fprintf(modelfile, "loc ri%d: invariant True\n", numvoter);
    for (countCandidates = 1; countCandidates <= nbcandidates;
         countCandidates++)
      fprintf(modelfile, "\twhen True sync vi%d%d goto voted%d%d;\n", numvoter,
              countCandidates, numvoter, countCandidates);
    fprintf(modelfile, "loc rp%d: invariant True\n", numvoter);
    for (countCandidates = 1; countCandidates <= nbcandidates;
         countCandidates++)
      fprintf(modelfile, "\twhen True sync vp%d%d goto voted%d%d;\n", numvoter,
              countCandidates, numvoter, countCandidates);
  }

  /* finished voting */
  fprintf(modelfile, "(* finished vote *)\n");
  for (countCandidates = 1; countCandidates <= nbcandidates; countCandidates++)
    /* if the voter is part of the agents in the formula,
    check the voting time */
    if (numvoter <= nbagents)
      fprintf(modelfile, "loc voted%d%d: invariant t<=8\n", numvoter,
              countCandidates);
    else
      fprintf(modelfile, "loc voted%d%d: invariant True\n", numvoter,
              countCandidates);
  fprintf(modelfile, "end\n\n");
} /* end writeVoter */

/* Write the Imitator property */
void writeImitatorProperty() {
  FILE *thefile;
  time_t current_time;
  char *c_time_string;
  int countAgents;

  thefile = fopen(propfilename, "w");
  /* write header */
  fprintf(thefile, "(* IMITATOR file automatically generated with *)\n");
  fprintf(thefile, "(* %s *)\n", cmdline);
  current_time = time(NULL);
  c_time_string = strtok(ctime(&current_time), "\n");
  fprintf(thefile, "(* Created: %s *)\n\n", c_time_string);
  /* Write the property */
  fprintf(thefile, "property := ");
  if (!strcmp(synthesis, "one"))
    fprintf(thefile, "#witness");
  else
    fprintf(thefile, "#synth");
  fprintf(thefile, " EF(loc[voter1] = voted11");
  for (countAgents = 2; countAgents <= nbagents; countAgents++)
    fprintf(thefile, " & loc[voter%d] = voted%d1", countAgents, countAgents);
  fprintf(thefile, ");\n");
  /* finished writing */
  fclose(thefile);
} /* end writeImitatorProperty */
