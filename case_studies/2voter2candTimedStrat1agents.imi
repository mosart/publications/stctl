(* Simple voters example *)

var t, x: clock; (* global time and delay *)
    (* Clock boundaries *)
    tstartm, tendm,
    tstarti, tendi,
    tstartp, tendp,
    (* strategies for voter 1 *)
    regm1, regi1, regp1,
    packm1, packi1, packp1,
    vm11, vi11, vp11,
    vm12, vi12, vp12
    : parameter;

(* The authority *)
automaton authority
synclabs:
    regm1, regi1, regp1,
    packm1, packi1, packp1,
    vm11, vi11, vp11,
    vm12, vi12, vp12,
    regm2, regi2, regp2,
    packm2, packi2, packp2,
    vm21, vi21, vp21,
    vm22, vi22, vp22,
    close
    ;

(* start by registering a modality for a voter *)
loc a0: invariant t <= tendp
    (* voter 1 *)
    when t < tstartm
        sync regm1 do {x:=0} goto m1; (* voter 1 by postal mail *)
    when t < tstarti
        sync regi1 do {x:=0} goto i1; (* voter 1 by internet *)
    when t < tstartp
        sync regp1 do {x:=0} goto p1; (* voter 1 at polling station *)
    (* voter 2 *)
    when t < tstartm
        sync regm2 do {x:=0} goto m2; (* voter 2 by postal mail *)
    when t < tstarti
        sync regi2 do {x:=0} goto i2; (* voter 2 by internet *)
    when t < tstartp
        sync regp2 do {x:=0} goto p2; (* voter 2 at polling station *)
(* receive votes *)
    (* voter 1 *)
    when tstartm <= t & t < tendm sync vm11 goto a0;
    when tstartm <= t & t < tendm sync vm12 goto a0;
    when tstarti <= t & t < tendi sync vi11 goto a0;
    when tstarti <= t & t < tendi sync vi12 goto a0;
    when tstartp <= t & t < tendp sync vp11 goto a0;
    when tstartp <= t & t < tendp sync vp12 goto a0;
    (* voter 2 *)
    when tstartm <= t & t < tendm sync vm21 goto a0;
    when tstartm <= t & t < tendm sync vm22 goto a0;
    when tstarti <= t & t < tendi sync vi21 goto a0;
    when tstarti <= t & t < tendi sync vi22 goto a0;
    when tstartp <= t & t < tendp sync vp21 goto a0;
    when tstartp <= t & t < tendp sync vp22 goto a0;
(* close the vote *)
    when t = tendp sync close goto closed;

(* send the voting package *)
(* voter 1 *)
loc m1: invariant True
    when x=0 sync packm1 goto a0;
loc i1: invariant True
    when x=0 sync packi1 goto a0;
loc p1: invariant True
    when x=0 sync packp1 goto a0;
(* voter 2 *)
loc m2: invariant True
    when x=0 sync packm2 goto a0;
loc i2: invariant True
    when x=0 sync packi2 goto a0;
loc p2: invariant True
    when x=0 sync packp2 goto a0;

(* the vote is closed *)
loc closed: invariant True
end

(* Voter 1 *)
automaton voter1
synclabs: regm1, regi1, regp1,
    packm1, packi1, packp1,
    vm11, vi11, vp11,
    vm12, vi12, vp12
    ;

(* start by registering a modality *)
loc v10: invariant True
    when regm1=1 & regi1=0 & regp1=0
        sync regm1 goto m1; (* vote by postal mail *)
    when regm1=0 & regi1=1 & regp1=0
        sync regi1 goto i1; (* vote by internet *)
    when regm1=0 & regi1=0 & regp1=1
        sync regp1 goto p1; (* vote at polling station *)

(* wait for voting package *)
loc m1: invariant True
    when packm1=1 sync packm1 goto rm1;
loc i1: invariant True
    when packi1=1 sync packi1 goto ri1;
loc p1: invariant True
    when packp1=1 sync packp1 goto rp1;

(* vote for candidate 1 or 2 *)
loc rm1: invariant True
    when vm11=1 & vm12=0 sync vm11 goto voted11;
    when vm11=0 & vm12=1 sync vm12 goto voted12;
loc ri1: invariant True
    when vi11=1 & vi12=0 sync vi11 goto voted11;
    when vi11=0 & vi12=1 sync vi12 goto voted12;
loc rp1: invariant True
    when vp11=1 & vp12=0 sync vp11 goto voted11;
    when vp11=0 & vp12=1 sync vp12 goto voted12;

(* final vote *)
loc voted11: invariant True
loc voted12: invariant True
end

(* Voter 2 *)
automaton voter2
synclabs: regm2, regi2, regp2,
    packm2, packi2, packp2,
    vm21, vi21, vp21,
    vm22, vi22, vp22
    ;

(* start by registering a modality *)
loc v20: invariant True
    when True sync regm2 goto m2; (* vote by postal mail *)
    when True sync regi2 goto i2; (* vote by internet *)
    when True sync regp2 goto p2; (* vote at polling station *)

(* wait for voting package *)
loc m2: invariant True
    when True sync packm2 goto rm2;
loc i2: invariant True
    when True sync packi2 goto ri2;
loc p2: invariant True
    when True sync packp2 goto rp2;

(* vote for candidate 1 or 2 *)
loc rm2: invariant True
    when True sync vm21 goto voted21;
    when True sync vm22 goto voted22;
loc ri2: invariant True
    when True sync vi21 goto voted21;
    when True sync vi22 goto voted22;
loc rp2: invariant True
    when True sync vp21 goto voted21;
    when True sync vp22 goto voted22;

(* final vote *)
loc voted21: invariant True
loc voted22: invariant True
end

init := {
    discrete =
        loc[authority] := a0,
        loc[voter1] := v10,
        loc[voter2] := v20;
    continuous =
        (* clocks *)
        x >= 0 & t = 0 &
        (* timing constraints *)
        tstartm >= 0 & tstartm <= tendm & (* tendm <= tendp & *)
        tstarti >= 0 & tstarti <= tendi & (* tendi <= tendp & *)
        tstartp >= 0 & tstartp <= tendp &
        (* strategies *)
        (* voter 1 *)
        regm1 >= 0 & regi1 >= 0 & regp1 >= 0 &
        packm1 >= 0 & packi1 >= 0 & packp1 >= 0 &
        vm11 >= 0 & vi11 >= 0 & vp11 >= 0 &
        vm12 >= 0 & vi12 >= 0 & vp12 >= 0
     ;
}
end
