(* Simple voters example *)

var t, x: clock; (* global time and delay *)
    (* Clock boundaries *)
    tstartm, tendm,
    tstarti, tendi,
    tstartp, tendp,
    (* strategies for voter 1 *)
    regm1, regi1, regp1,
    packm1, packi1, packp1,
    vm11, vi11, vp11,
    vm12, vi12, vp12
    : parameter;

(* The authority *)
automaton authority
synclabs:
    regm1, regi1, regp1,
    packm1, packi1, packp1,
    vm11, vi11, vp11,
    vm12, vi12, vp12,
    close
    ;

(* start by registering a modality for a voter *)
loc a0: invariant t <= tendp
    when t < tstartm
        sync regm1 do {x:=0} goto m1; (* voter 1 by postal mail *)
    when t < tstarti
        sync regi1 do {x:=0} goto i1; (* voter 1 by internet *)
    when t < tstartp
        sync regp1 do {x:=0} goto p1; (* voter 1 at polling station *)
(* receive votes *)
    when tstartm <= t & t < tendm sync vm11 goto a0;
    when tstartm <= t & t < tendm sync vm12 goto a0;
    when tstarti <= t & t < tendi sync vi11 goto a0;
    when tstarti <= t & t < tendi sync vi12 goto a0;
    when tstartp <= t & t < tendp sync vp11 goto a0;
    when tstartp <= t & t < tendp sync vp12 goto a0;
(* close the vote *)
    when t = tendp sync close goto closed;

(* send the voting package *)
loc m1: invariant True
    when x=0 sync packm1 goto a0;
loc i1: invariant True
    when x=0 sync packi1 goto a0;
loc p1: invariant True
    when x=0 sync packp1 goto a0;

(* the vote is closed *)
loc closed: invariant True
end

(* A voter *)
automaton voter1
synclabs: regm1, regi1, regp1,
    packm1, packi1, packp1,
    vm11, vi11, vp11,
    vm12, vi12, vp12
    ;

(* start by registering a modality *)
loc v10: invariant True
    when regm1=1 & regi1=0 & regp1=0
        sync regm1 goto m1; (* vote by postal mail *)
    when regm1=0 & regi1=1 & regp1=0
        sync regi1 goto i1; (* vote by internet *)
    when regm1=0 & regi1=0 & regp1=1
        sync regp1 goto p1; (* vote at polling station *)

(* wait for voting package *)
loc m1: invariant True
    when packm1=1 sync packm1 goto rm1;
loc i1: invariant True
    when packi1=1 sync packi1 goto ri1;
loc p1: invariant True
    when packp1=1 sync packp1 goto rp1;

(* vote for candidate 1 or 2 *)
loc rm1: invariant True
    when vm11=1 & vm12=0 sync vm11 goto voted11;
    when vm11=0 & vm12=1 sync vm12 goto voted12;
loc ri1: invariant True
    when vi11=1 & vi12=0 sync vi11 goto voted11;
    when vi11=0 & vi12=1 sync vi12 goto voted12;
loc rp1: invariant True
    when vp11=1 & vp12=0 sync vp11 goto voted11;
    when vp11=0 & vp12=1 sync vp12 goto voted12;

(* final vote *)
loc voted11: invariant True
loc voted12: invariant True
end

init := {
    discrete =
        loc[authority] := a0,
        loc[voter1] := v10;
    continuous =
        (* clocks *)
        x >= 0 & t = 0 &
        (* timing constraints *)
        tstartm >= 0 & tstartm <= tendm & (* tendm <= tendp & *)
        tstarti >= 0 & tstarti <= tendi & (* tendi <= tendp & *)
        tstartp >= 0 & tstartp <= tendp &
        (* strategies *)
        regm1 >= 0 & regi1 >= 0 & regp1 >= 0 &
        packm1 >= 0 & packi1 >= 0 & packp1 >= 0 &
        vm11 >= 0 & vi11 >= 0 & vp11 >= 0 &
        vm12 >= 0 & vi12 >= 0 & vp12 >= 0
     ;
}
end
