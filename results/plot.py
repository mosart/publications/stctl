#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import plotly.express as px
import plotly.io as pio   

pio.kaleido.scope.mathjax = None
pio.kaleido.scope.default_width=1800


# In[2]:


def read_csv(filename):
    df = pd.read_csv(filename, sep=";", skipinitialspace=True, skiprows=2, na_values="TO")
    df_melt = pd.melt(df, id_vars=["agents", "strategy", "timeout", "voters"], var_name="candidate", value_name="time")
    df_filter = df_melt.groupby(['agents','voters']).filter(lambda g: g.time.notnull().sum() > 0).reset_index(drop=True)
    
    return df_filter


# In[3]:


def plot(df, title):
    fig =px.bar(df, 
                    x="voters", y="time", color="candidate",
                    facet_col="agents", barmode="group",
                    title=title,
                    labels={
                        "time": "time (s)",
                        "voters": "# voters",
                        "agents": "# agents",
                        "candidate": "# candidates"
                    })
    for trace in fig["data"]:
        trace["name"] = trace["name"].replace('candidate_',"")

    # legend layout
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="left",
        x=0.01,
        bgcolor="white",
        bordercolor="grey",
        borderwidth=1
    ))
    
    # title layout
    fig.update_layout(title={
        'y':0.9,
        'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top'
    })
    
    # figure layout
    fig.update_layout(plot_bgcolor="white", paper_bgcolor="white", bargap=0.2)
    
    # bars layout
    fig.update_traces(marker={"line": {"width": 1, "color": "rgb(0,0,0)"}})
    
    fig.update_xaxes(showline=True, linewidth=1, linecolor='black', mirror=True, dtick=1, matches=None)
    fig.update_yaxes(showline=True, linewidth=1, linecolor='black', mirror=True, dtick=10, showgrid=True, gridwidth=1, gridcolor='LightGrey', griddash='dash')

    return fig


# In[4]:


def save_plot(fig, name):
    fig.write_image(f"{name}.png")
    fig.write_image(f"{name}.pdf")
    fig.write_html(f"{name}.html")


# # Model Checking

# In[5]:


df_one = read_csv("results_one.csv")
# df_one


# In[6]:


fig_one = plot(df_one, "Model Checking")
save_plot(fig_one, "results_one")
fig_one


# # Synthesising All Strategies

# In[7]:


df_all = read_csv("results_all.csv")
# df_all


# In[8]:


fig_all = plot(df_all, "Synthesising All Strategies")
save_plot(fig_all, "results_all")
fig_all


# In[ ]:




